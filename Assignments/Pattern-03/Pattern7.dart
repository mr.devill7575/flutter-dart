/*
Rows=4
1
3 5
5 7 9
7 9 11 13
*/

import 'dart:io';
void main(){
        int row= 4;
        int num= 1;
        for (int i=1; i<=row; i++){
		int temp=num;
                for(int j=1; j<=i; j++){
                        stdout.write('$temp ');
                        temp+=2;
                }
		num+=2;
                stdout.writeln();
        }
}
