/*
1
3 4
5 6 8
7 8 10 13
*/
import 'dart:io';
void main(){
        int row= 4;
	int num= 1;

        for (int i=1; i<=row; i++){
                int temp= num;
		for(int j=1; j<=i; j++){
                        stdout.write('$temp ');
			temp+=j;
                }
		num+=2;
                stdout.writeln();
        }
}
