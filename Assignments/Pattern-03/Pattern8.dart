/*
Rows = 5
5
6 8
7 10 13
8 12 16 20
9 14 19 24 29
*/
import 'dart:io';
void main(){
        int row= 5;
	int num= row;

        for (int i=1; i<=row; i++){
                int temp= num;
		for(int j=1; j<=i; j++){
                        stdout.write('$temp ');
			temp+=i;
                }
		num++;
                stdout.writeln();
        }
}
