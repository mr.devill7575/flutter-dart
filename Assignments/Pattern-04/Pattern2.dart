/*
rows=4
1 1 1 1
2 2 2
3 3
4
*/

import 'dart:io';
void main(){
        int row= 4;
	int num=1;
        for (int i=row; i>=1; i--){
                for(int j=1; j<=i; j++){
                        stdout.write("$num ");
                }
		num++;
                stdout.writeln();
        }
}
