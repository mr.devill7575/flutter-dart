/*
rows =3
1 2 3
1 2
1
*/

import 'dart:io';
void main(){
        int row= 3;

        for (int i=row; i>=1; i--){
                for(int j=1; j<=i; j++){
                        stdout.write('$j ');
                }
                stdout.writeln();
        }
}
