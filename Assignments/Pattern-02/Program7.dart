/*
1  2  3  4
4  5  6  7
7  8  9  10
10 11 12 13
*/

import 'dart:io';
void main(){
        int row= 4;
	int temp=1;
        for (int i=1; i<=row; i++){
		int num = temp;
                for(int j=1; j<=row; j++){
                        stdout.write('$num ');
			temp=num;
			num++;
                }
                stdout.writeln();

        }
}
