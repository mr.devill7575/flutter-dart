/*
1  2  3  4
5  6  7  8 
9  10 11 12
13 14 15 16
*/
import 'dart:io';
void main(){
        int row= 4;
	int num= 1;
        for (int i=1; i<=row; i++){
                for(int j=1; j<=row; j++){
                        stdout.write('$num ');
			num++;
                }
                stdout.writeln();
        }
}
